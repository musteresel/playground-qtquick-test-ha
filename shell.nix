{ pkgs ? import <nixpkgs> {}, test-ha ? pkgs.libsForQt5.callPackage ./. {} }:

pkgs.mkShell {
  inputsFrom = [ test-ha ];
  buildInputs = with pkgs; [
    qtcreator
    qt5Full
  ];
}
