# Roos' QtQuick playground for developing a simple home automation app

The purpose of this repo is to get myself started with QtQuick.
Ideally I should end up doing something like https://www.youtube.com/watch?v=hJKVb7WpQ_c

This project is built using [Nix](https://nixos.org/).

## How to build

Run `nix build`. If this command is not available, use the old `nix-build`.

## How to develop

There are two ways to enter a development shell for this project.

#### Using nix flakes

Nix flakes provide reproductible evalutaions.
The reasoning behind this is that different releases of nixpkgs may provide
different versions of software.
Which release is used is dependent on the system configuration running the
build.

Ideally, all releases of nixpkgs should work.
In reality, a package may be broken in a given release (for example, at the time
of writing, 20.09 is in beta but I've made it the default channel in my personal
laptop; it's derivation of qt5Full is currently broken).
Flakes resolve that issue by pinning what release for nixpkgs should be used to
evaluate this project.

You may enter a development environment using `nix develop`.

Note that _Nix Flakes_ are an experimental feature and must be enabled before
using.
If you prefer to use the legacy system, please continue reading.

Note: the flake environment will be generated from the project's `shell.nix` for
compatibility reasons.

#### Using nix-shell

The standard `shell.nix` file may also be used to enter a development
environment.

You may enter a development environment using `nix-shell`.
